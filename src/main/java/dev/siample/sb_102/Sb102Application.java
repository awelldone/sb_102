package dev.siample.sb_102;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

//@Configuration
//@EnableAutoConfiguration
//@ComponentScan
@SpringBootApplication
public class Sb102Application {

	public static void main(String[] args) {
		ApplicationContext aContext = SpringApplication.run(Sb102Application.class, args);

		String[] beanArray = aContext.getBeanDefinitionNames();
		Arrays.sort(beanArray);

		for (String name : beanArray) {
			System.out.println(name);
		}
	}
}
