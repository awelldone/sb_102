package dev.siample.sb_102;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("prototype") // Be session, not singleton after the always singleton-(Rest)controller!
public class SpyGirl {

	public String iSaySg() {
		return "The spring bean-s are in the aContext!";
	}
}
